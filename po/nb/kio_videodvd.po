# Translation of kio_videodvd to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_videodvd\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:30+0000\n"
"PO-Revision-Date: 2009-05-06 18:12+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: videodvd.cpp:163 videodvd.cpp:291
#, kde-format
msgid "No Video DVD found"
msgstr "Fant ingen video-DVD"

#: videodvd.cpp:206 videodvd.cpp:374
#, kde-format
msgid "Read error."
msgstr "Lesefeil."
