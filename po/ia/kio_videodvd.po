# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the k3b package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: k3b\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:30+0000\n"
"PO-Revision-Date: 2020-09-10 23:33+0100\n"
"Last-Translator: Giovanni Sora <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: videodvd.cpp:163 videodvd.cpp:291
#, kde-format
msgid "No Video DVD found"
msgstr "Necun DVD video trovate"

#: videodvd.cpp:206 videodvd.cpp:374
#, kde-format
msgid "Read error."
msgstr "Error de lectura."
